generate:
	protoc -I. --go_out=plugins=micro:. proto/time/time.proto

build: generate
	docker build -t time_service .
	# push to docker hub?

run:
	docker run -it \
		--rm --name "time_service" \
		-p 50056 \
		-e DB_HOST="mongodb://user:test123@alpaca-shard-00-00-cz5qu.mongodb.net:27017,alpaca-shard-00-01-cz5qu.mongodb.net:27017,alpaca-shard-00-02-cz5qu.mongodb.net:27017/test?ssl=true&replicaSet=Alpaca-shard-0&authSource=admin&retryWrites=true" \
		-e MICRO_SERVER_ADDRESS=:50056 \
		-e MICRO_REGISTRY=mdns \
		-e DISABLE_AUTH=true \
		time_service

release:
	docker build -t registry.gitlab.com/lazybasterds/alpaca/time-service:0.1.1 .
	docker push registry.gitlab.com/lazybasterds/alpaca/time-service:0.1.1
