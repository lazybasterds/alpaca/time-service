package main

//Time contains the struct of World time from http://worldtimeapi.org/
type Time struct {
	ID           string      `json:"id"`
	WeekNumber   string      `json:"week_number"`
	UTCOffset    string      `json:"utc_offset"`
	UnixTime     string      `json:"unixtime"`
	TimeZone     string      `json:"timezone"`
	DSTUntil     interface{} `json:"dst_until"`
	DSTFrom      interface{} `json:"dst_from"`
	DST          bool        `json:"dst"`
	DayOfYear    int32       `json:"day_of_year"`
	DayOfWeek    int32       `json:"day_of_week"`
	DateTime     string      `json:"datetime"`
	Abbreviation string      `json:"abbreviation"`
}
