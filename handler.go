package main

import (
	"context"
	"log"

	"github.com/micro/go-micro/metadata"
	pb "gitlab.com/lazybasterds/alpaca/time-service/proto/time"
)

type service struct {
}

func (s *service) GetRepo() Repository {
	return &TimeRepository{}
}

func (s *service) GetCurrentTime(ctx context.Context, req *pb.Time, res *pb.TimeResponse) error {
	log.Println(ctx)
	repo := s.GetRepo()

	mdata, ok := metadata.FromContext(ctx)
	var requestID string
	if ok {
		requestID = mdata["requestid"]
	}

	timezone := req.Timezone
	currentTime, err := repo.GetCurrentTime(ctx, timezone)
	if err != nil {
		log.Printf("Error: Failed to get current time : %+v", err)
		return err
	}
	log.Printf("Method: GetCurrentTime, Requestid: %s, Return: %#v", requestID, currentTime)
	res.Time = currentTime
	return nil
}
