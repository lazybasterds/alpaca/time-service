package main

import (
	"context"
	"encoding/json"
	"os/exec"

	pb "gitlab.com/lazybasterds/alpaca/time-service/proto/time"
)

//Repository is an interface for getting information from the database.
type Repository interface {
	GetCurrentTime(ctx context.Context, timezone string) (*pb.Time, error)
}

//TimeRepository concrete implementation of the Repository interface.
type TimeRepository struct {
}

// GetCurrentTime retrieves the time from the http://worldtimeapi.org given the timezone.
// Example of timezone would be: Europe/Berlin, Asia/Singapore, Asia/Manila
func (repo *TimeRepository) GetCurrentTime(ctx context.Context, timezone string) (*pb.Time, error) {
	requestURL := "http://worldtimeapi.org/api/timezone/" + timezone

	curl := exec.Command("curl", requestURL)
	out, err := curl.Output()

	if err != nil {
		return nil, err
	}

	var time Time
	err = json.Unmarshal(out, &time)
	if err != nil {
		return nil, err
	}

	//The World Time API does not provide any ID and the client is not using the ID (so far), so just give a dummy ID
	time.ID = "1"

	dstUntil, _ := time.DSTUntil.(string)
	dstFrom, _ := time.DSTFrom.(string)

	return &pb.Time{
		Id:           time.ID,
		Weeknumber:   time.WeekNumber,
		Utcoffset:    time.UTCOffset,
		Unixtime:     time.UnixTime,
		Timezone:     time.TimeZone,
		Dstuntil:     dstUntil,
		Dstfrom:      dstFrom,
		Dst:          time.DST,
		Dayofyear:    time.DayOfYear,
		Dayofweek:    time.DayOfWeek,
		Datetime:     time.DateTime,
		Abbreviation: time.Abbreviation,
	}, nil
}
